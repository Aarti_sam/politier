package com.knackter.demo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.knackter.demo.R;

public class SplashActivity extends AppCompatActivity {

    ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        imageView = (ImageView)findViewById(R.id.img);

        Thread timer = new Thread() {

            public void run() {
                try {
                    sleep(1000);
                } catch (Exception e) {
                    // Log.e(TAG, e.getMessage());
                } finally {
                    Intent newActivity = new Intent(SplashActivity.this,
                            LoginActivity.class);
                    startActivity(newActivity);
                    finish();
                }
            }
        };
        timer.start();

    }
}
