package com.knackter.demo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.knackter.demo.R;

public class SignupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
    }

    public void Login(View v) {

        Intent i = new Intent(SignupActivity.this, LoginActivity.class);
        startActivity(i);
    }

    public void Link(View v) {

        Intent i = new Intent(SignupActivity.this, LoginActivity.class);
        startActivity(i);
    }

}
