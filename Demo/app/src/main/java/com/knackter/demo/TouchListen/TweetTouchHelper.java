package com.knackter.demo.TouchListen;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.knackter.demo.adapter.TweetDataAdapter;
import com.knackter.demo.rest.ApiInterface;
import com.knackter.demo.rest.Apiclient2;

/**
 * Created by RUYEE on 3/7/2017.
 */

public class TweetTouchHelper extends ItemTouchHelper.SimpleCallback {

    private TweetDataAdapter adpter;
    private ApiInterface apiService = Apiclient2.getClient().create(ApiInterface.class);

    public TweetTouchHelper(TweetDataAdapter tweetDataAdapter){
        super(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        this.adpter = tweetDataAdapter;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        adpter.swap(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        adpter.remove(viewHolder.getAdapterPosition());
    }



}
