package com.knackter.demo.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RUYEE on 3/5/2017.
 */

public class Apiclient2 {

    public static final String BASE_URL = "http://103.3.61.119:8121/politier/";
    private static Retrofit retrofit = null;




    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
