package com.knackter.demo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.knackter.demo.Activity.MainActivity;
import com.knackter.demo.R;
import com.knackter.demo.model.TweetData;

import java.util.Collections;
import java.util.List;

/**
 * Created by RUYEE on 2/27/2017.
 */

public class TweetDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int TYPE_TWEET = 0;
    private final int TYPE_LOAD = 1;

    private Context context;
    private List<TweetData> tweets;
    private OnLoadMoreListener loadMoreListener;
    private boolean isLoading = false, isMoreDataAvailable = false;

    /*
    * isLoading - to set the remote loading and complete status to fix back to back load more call
    * isMoreDataAvailable - to set whether more data from server available or not.
    * It will prevent useless load more request even after all the server data loaded
    * */
    @Override
    public long getItemId(int position) {
        return position;

    }
    public TweetDataAdapter(Context context, List<TweetData> tweets) {
        this.context = context;
        this.tweets = tweets;
        setHasStableIds(true);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);

 if (viewType == TYPE_TWEET) {
            return new TweetdataViewHolder(inflater.inflate(R.layout.list_tweet, parent, false));

        }else{
            return new LoadHolder(inflater.inflate(R.layout.row_load,parent,false));
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

       if(position >= getItemCount() && isMoreDataAvailable && !isLoading && loadMoreListener!=null){
            isLoading = true;
            loadMoreListener.onLoadMore();


        }

        if(getItemViewType(position) == TYPE_TWEET){
            ((TweetdataViewHolder)holder).bindData(tweets.get(position));
        }
        //No else part needed as load holder doesn't bind any data

    }

    @Override

   public int getItemViewType(int position) {
        if(tweets.get(position) != null) {
            return TYPE_TWEET;
        }
        else{
            return TYPE_LOAD;
        }

    }


  public void remove(int position) {
        tweets.remove(position);
        notifyItemRemoved(position);

      TweetData tweetData = tweets.get(position);
      Intent intent = new Intent(context, MainActivity.class);
      intent.putExtra("tweetId",tweetData.getId());
      context.startActivity(intent);
    }

    public void swap(int firstPosition,  int secondPosition){
        Collections.swap(tweets, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }


    @Override
    public int getItemCount() {
        return tweets.size();
    }

    /* VIEW HOLDERS */

    private static class TweetdataViewHolder extends RecyclerView.ViewHolder{
        TextView tweetId;
        TextView content;
        TextView creationTime;
        TextView likeCount;
        TweetdataViewHolder(View itemView) {
            super(itemView);
            tweetId = (TextView) itemView.findViewById(R.id.tweetId1 );
            content = (TextView) itemView.findViewById(R.id.content1);
            creationTime = (TextView) itemView.findViewById(R.id.creationTime1);
            likeCount = (TextView) itemView.findViewById(R.id.likeCount1);  }

        @SuppressLint("SetTextI18n")
        void bindData(TweetData tweets){
            tweetId.setText(tweets.getId().toString());
            content.setText(tweets.getContent());
            creationTime.setText(tweets.getCreationTime());
            likeCount.setText(tweets.getLikeCount().toString());
        }
    }

    private static class LoadHolder extends RecyclerView.ViewHolder{
        LoadHolder(View itemView) {
            super(itemView);
        }
    }

   public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }
/*
     notifyDataSetChanged is final method so we can't override it
         call adapter.notifyDataChanged(); after update the list
         */
    public final void notifyDataChanged(){
        notifyDataSetChanged();
        isLoading = false;
    }


   public interface OnLoadMoreListener{
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }



}

