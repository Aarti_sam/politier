package com.knackter.demo.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.knackter.demo.R;
import com.knackter.demo.TouchListen.TweetTouchHelper;
import com.knackter.demo.adapter.TweetDataAdapter;
import com.knackter.demo.model.TweetData;
import com.knackter.demo.rest.ApiClient;
import com.knackter.demo.rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<TweetData> tweets;
    TweetDataAdapter adapter;
    ApiInterface api;
    String TAG = "MainActivity - ";
    Context context;
    int tweetId;
    int pageNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        tweets = new ArrayList<>();

        adapter = new TweetDataAdapter(this, tweets);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);



        ItemTouchHelper.Callback callback = new TweetTouchHelper(adapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(recyclerView);


        api = ApiClient.createService(ApiInterface.class);

        //int pageNumber;
        for (pageNumber = 1; pageNumber <= 6; pageNumber++) {
            load();

        }


    }

    private void load() {
        Call<List<TweetData>> call = api.getTweet(pageNumber);
        call.enqueue(new Callback<List<TweetData>>() {

            @Override
            public void onResponse(Call<List<TweetData>> call, Response<List<TweetData>> response) {

                if (response.isSuccessful()) {

                   // List<TweetData> tweetDataList = response.body();
                    tweets.addAll(response.body());
                    adapter.notifyDataChanged();
                    recyclerView.setHasFixedSize(true);

                   // adapter = new TweetDataAdapter(MainActivity.this, tweets);
                   // recyclerView.setAdapter(adapter);
                    //adapter.notifyDataChanged();

                    Log.i(TAG, "Getting Response  " + tweets.addAll(response.body()));
                } else {
                    Log.e(TAG, " Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<List<TweetData>> call, Throwable t) {
                Log.e(TAG, " Response Error " + t.getMessage());
            }
        });



        Intent intent= getIntent();
        tweetId = intent.getIntExtra("tweetId",0);


        final Call<Void> deleteItem = api.deleteItem(tweetId);
        Log.i(TAG,"########  responce :" + tweetId);
        deleteItem.enqueue(new Callback<Void>()

        {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if (response.isSuccessful()) {
                    //Log.d(TAG, "response code" + response.body().toString());
                    //Log.i(TAG, "Deleted from  API." + response.body().toString());
                    //Toast.makeText(MainActivity.this, "Post Deleted", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(TAG, "Unable to Delete post to API.");
            }
        });
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.logout) {
            logout();
            return true;
        }

        if (id == R.id.create_tweet) {
            post();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

   private void post() {

       startActivity(new Intent(MainActivity.this , PostActivity.class)) ;

    }

    private void logout() {
        SharedPreferences sp=getSharedPreferences("login",MODE_PRIVATE);
        SharedPreferences.Editor e=sp.edit();
        e.clear();
        e.apply();

        startActivity(new Intent(MainActivity.this,LoginActivity.class));
        finish();   //finish current activity
    }




}